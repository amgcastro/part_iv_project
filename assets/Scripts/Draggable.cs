﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Draggable : MonoBehaviour
{

	//private Vector2 screenPoint;
	private bool rightIsPressed = false;
	public GameObject tabletop;

	void OnMouseDown()
	{
		Debug.Log("OnMouseDown");
		//screenPoint = Camera.main.WorldToScreenPoint (this.transform.position);
		//offSet = this.transform.position - Camera.main.ScreenToWorldPoint (new Vector2 (Input.mousePosition.x, Input.mousePosition.y
	}

	void OnMouseDrag()
	{
		float currentZ = this.transform.position.z;
		float currentZRotation = this.transform.rotation.z;

		Vector2 currentScreenPoint = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		Vector2 currentPosition = Camera.main.ScreenToWorldPoint (currentScreenPoint);
		this.transform.position = new Vector3(currentPosition.x, currentPosition.y, currentZ);

		if(Input.GetMouseButtonDown(1))
		{
			if(!this.rightIsPressed)
			{
				this.rightIsPressed = true;
				if(currentZRotation == 0)
				{
					currentZRotation = -90;
					this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, currentZRotation, 90f);
				}
				else
				{
					currentZRotation = 0;
					this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, currentZRotation, 90f);
				}
			}
		}
		else if(Input.GetMouseButtonUp(1))
		{
			this.rightIsPressed = false;
		}
	}

	void OnMouseUp()
	{
		Debug.Log("OnMouseUp");
	}
		
		
}
