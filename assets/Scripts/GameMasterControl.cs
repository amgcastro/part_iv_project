﻿using UnityEngine;
using System.Collections;

public class GameMasterControl : MonoBehaviour {

	public BoxCollider2D topWall, bottomWall, leftWall, rightWall;
	public Camera mainCam;

	// Use this for initialization
	void Start () 
	{
		/*initialise the boundaries*/
		setWallPosition ();
		setWallDimensions ();
		/**************************/
	}

	private void setWallPosition()
	{
		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint(new Vector3(Screen.width * 2f, 0f, 0f)).x, 1f);
		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint(new Vector3(Screen.width * 2f, 0f, 0f)).x, 1f);
		leftWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height * 2f, 0f)).y);
		rightWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height * 2f, 0f)).y);
	}

	private void setWallDimensions()
	{
		topWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y + 0.5f);
		bottomWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).y - 0.5f);
		leftWall.offset = new Vector2 (mainCam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).x - 0.5f, 0f);
		rightWall.offset = new Vector2 (mainCam.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x + 0.5f, 0f);
	}

}
