using UnityEngine;
using System.Collections;

public class TableTopControl : MonoBehaviour {

	public GameObject[] otherTerminals;
	bool canSnapFlag = false;

	// Use this for initialization
	void Start () 
	{
		otherTerminals = GameObject.FindGameObjectsWithTag("Terminal");

		Debug.Log (otherTerminals.Length);
		for(int i = 0; i < otherTerminals.Length; i++)
		{
			Debug.Log (otherTerminals [i]);
		}
	}

	void Snap(Transform resistor)
	{
		float distance;

		for (int i = 0; i < otherTerminals.Length - 1; i++) 
		{
			for (int j = i + 1; j < otherTerminals.Length; j++)
			{
				distance = Vector2.Distance (otherTerminals [i].transform.position, otherTerminals [j].transform.position);

				if (distance <= 1) 
				{
					Debug.Log("Snapping");
					if(otherTerminals [i].transform.parent.name == resistor.name)
					{
						otherTerminals [i].transform.parent.position = otherTerminals [j].transform.parent.position;
					}
					else if (otherTerminals [j].transform.parent.name == resistor.name)
					{
						otherTerminals [j].transform.parent.position = otherTerminals [i].transform.parent.position;
					}
				}
			}
		}

				
	}
		

}
