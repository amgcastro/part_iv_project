﻿using UnityEngine;
using System.Collections;

public class TerminalEndSnap : MonoBehaviour {

	public GameObject[] otherTerminals;
	public GameObject tableTop;


	// Use this for initialization
	void Start ()
	{
		otherTerminals = GameObject.FindGameObjectsWithTag("Terminal");

		Debug.Log (otherTerminals.Length);
		for(int i = 0; i < otherTerminals.Length; i++)
		{
			Debug.Log (otherTerminals [i]);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		float distance;

		for (int i = 0; i < otherTerminals.Length; i++)
		{
			if(otherTerminals[i].name == this.name || otherTerminals[i].transform.parent == this.transform.parent)
			{
				continue;
			}
			else
			{
				distance = Vector2.Distance (otherTerminals [i].transform.position, this.transform.position);

				if(distance <= 1)
				{
					//Debug.Log (this.ToString () + " is close to " + otherTerminals [i].transform.ToString ());
					if(Input.GetMouseButtonUp(0))
					{
						//Debug.Log (this.transform.parent.ToString());
						tableTop.SendMessage ("Snap", this.transform.parent);
					}
				}
			}
		}

	}
}
